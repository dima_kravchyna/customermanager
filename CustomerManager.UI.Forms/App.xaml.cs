﻿using CustomerManager.Core;
using CustomerManager.Core.Services;
using CustomerManager.Core.ViewModels;
using CustomerManager.UI.Forms.Pages;
using CustomerManager.UI.Forms.Services;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Xamarin.Forms;

namespace CustomerManager.UI.Forms
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var nav = new XamFormsNavigationService();
			SimpleIoc.Default.Register<INavigationService>(() => nav);

            nav.Configure(MessageData.MainPageName, typeof(MainPage));
            nav.Configure(MessageData.CustomerListPageName, typeof(CustomerListPage));
            nav.Configure(MessageData.CustomerInfoPageName, typeof(CustomerPage));
			nav.Configure(MessageData.CustomerEditionPageName, typeof(CustomerEditPage));

            var firstPage = new NavigationPage(new MainPage());
            nav.Initialize(firstPage);
            MainPage = firstPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
