﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerManager.Core.Models;

namespace CustomerManager.Core.Services
{
	public class TestCustomerRepository : ICustomerRepository
	{
		#region test data
	    private List<Customer> _storage = new List<Customer>
	    {
            new Customer
            {
                Id = "42d32dc3-d7bf-487b-901d-0e58584c1a7f",
                Name = "Ivan",
                LastName = "Ivanovenkovich",
                Position = "Software Developer",
                Company = "Xamarin HQ",
                Photo = "http://www.refractored.com/images/SF.png",
                PhoneNumber = "855-916-2743",
                EMail = "ivanov@xam.com",
                Skype = "ivanov.ivan",
                StreetAddress = "394 Pacific Ave. 4th Floor",
                City = "San Francisco",
                State = "CA",
                Country = "United States",
                ZipCode = "94111",
                Latitude = 37.797788,
                Longitude = -122.401858,
                AboutUs = "Sansome & Pacific"
            },
	        new Customer
	        {
	            Id = "94fa54ab-72c2-40c7-85ee-491f2c9d023e",
	            Name = "Sarah",
	            LastName = "Connor",
	            Position = "Security Manager",
	            Company = "Xamarin Inc. Argentina",
	            Photo = "http://www.refractored.com/images/SF.png",
	            PhoneNumber = "855-926-2746",
	            EMail = "sarah.connor@mail.com",
	            Skype = "",
	            StreetAddress = "Av. Pres. Roque Saenz Pena 875",
	            City = "Buenos Aires",
	            State = "",
	            Country = "Argentina",
	            ZipCode = "C1035AAD CABA",
	            Latitude = -34.6049956875424,
	            Longitude = -58.3788027544727,
	            AboutUs = "Visual Studio to the Max!"
	        }
	    };
        #endregion

        public async Task<Customer> GetByIdAsync(string id)
        {
            return _storage.FirstOrDefault(t => string.Equals(t.Id, id));
        }
        public async Task<IEnumerable<Customer>> GetAllAsync()
		{
			_storage =  new List<Customer>(_storage);
			return  _storage;
		}
	    public async Task<Customer> AddAsync(Customer customer)
		{
			return customer;
		}
		public async Task<bool> RemoveAsync(Customer customer)
		{
		    _storage.Remove(customer);
            return true;
		}
		public async Task<Customer> UpdateAsync(Customer customer)
		{
			for (int i = 0; i < _storage.Count; i++)
				if (string.Equals(_storage[i].Id, customer.Id))
				{
					_storage[i] = customer;
					return customer;
				}
			return null;
		}
	}
}