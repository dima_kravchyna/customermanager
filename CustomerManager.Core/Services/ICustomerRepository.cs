﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CustomerManager.Core.Models;

namespace CustomerManager.Core.Services
{
	public interface ICustomerRepository
	{
		Task<Customer> GetByIdAsync(string id);
        Task<IEnumerable<Customer>> GetAllAsync();
        Task<Customer> AddAsync(Customer customer);
		Task<bool> RemoveAsync(Customer customer);
		Task<Customer> UpdateAsync(Customer customer);
	}
}