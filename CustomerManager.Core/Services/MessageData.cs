﻿namespace CustomerManager.Core.Services
{
	public class MessageData
	{
	    public const string MainPageName = "MainPage";
	    public const string CustomerListPageName = "CustomerListPage";
	    public const string CustomerInfoPageName = "CustomerInfoPage";
	    public const string CustomerEditionPageName = "CustomerEditionPage";

        public bool UpdateRequired { get; set; }
        public bool SortRequired { get; set; }

        public MessageData(bool updateRequired = false, bool sortRequired = false)
        {
            UpdateRequired = updateRequired;
            SortRequired = sortRequired;
        }
	}
}