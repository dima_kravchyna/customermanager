﻿using CustomerManager.Core.Services;
using GalaSoft.MvvmLight.Command;

namespace CustomerManager.Core.ViewModels
{
    public class MainViewModel : BaseViewModel
	{
	    private RelayCommand _goToCustomers;
		public RelayCommand GoToCustomersCommand
		{
		    get
		    {
		        return _goToCustomers ??
		               (_goToCustomers = new RelayCommand(GoToCustomers));
		    }
		}

        private void GoToCustomers()
		{
			NavigatioService.NavigateTo(MessageData.CustomerListPageName);
		}
	}
}