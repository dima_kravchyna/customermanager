﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using CustomerManager.Core.Helpers;
using CustomerManager.Core.Models;
using CustomerManager.Core.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace CustomerManager.Core.ViewModels
{
	public class CustomerListViewModel : BaseViewModel
	{
		private readonly ICustomerRepository _customerRepository;
        private ObservableCollection<Grouping<string, ReactiveCustomer>> _customersGrouped = new ObservableCollection<Grouping<string, ReactiveCustomer>>();
        private List<CustomerViewModel> _customers = new List<CustomerViewModel>();

        private RelayCommand _getCustomersCommand;
        private RelayCommand<CustomerViewModel> _goToCustomerCommand;

	    public ObservableCollection<Grouping<string, ReactiveCustomer>> CustomersGrouped
	    {
	        get { return _customersGrouped; }
	        set { Set(() => CustomersGrouped, ref _customersGrouped, value); }
	    }

	    public RelayCommand GetCustomersCommand
		{
			get
			{
				return _getCustomersCommand ?? 
                      (_getCustomersCommand = new RelayCommand(async () => await GetCustomers(), () => !IsBusy));
			}
		}
		public RelayCommand<CustomerViewModel> GoToCustomerCommand
		{
			get
			{
				return _goToCustomerCommand ??
					  (_goToCustomerCommand = new RelayCommand<CustomerViewModel>(GoToCustomer, customer => customer != null));
			}
		}

        private async Task GetCustomers()
		{
			if (IsBusy)
				return;

			IsBusy = true;
			GetCustomersCommand.RaiseCanExecuteChanged();
			try
			{
                var models = await _customerRepository.GetAllAsync();
			    _customers = models.Select(customer =>
			    {
			        return new CustomerViewModel(_customerRepository,
			            async reactiveCustomer => await DeleteCustomer(reactiveCustomer),
			            reactiveCustomer => !IsBusy) {Model = customer};
			    }).ToList();
			    Sort();
			}
			catch (Exception)
			{
				// DisplayAlert("error", "OK");
			}
			finally
			{
				IsBusy = false;
				GetCustomersCommand.RaiseCanExecuteChanged();
			}
		}
        private void GoToCustomer(CustomerViewModel customerViewModel)
        {
            ViewModelLocator.Locator.Customer = customerViewModel;
            NavigatioService.NavigateTo(MessageData.CustomerInfoPageName);
        }
        private async Task DeleteCustomer(CustomerViewModel customer)
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                await _customerRepository.RemoveAsync(customer.Model);
                _customers.Remove(customer);
                Sort();
            }
            catch (Exception)
            {
                // DisplayAlert("Unable to remove customer, please try again", "OK");
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void Sort()
		{
            var sorted = from customer in _customers
                         orderby customer.Country, customer.LastName
			group customer by customer.Country
			into sustomerGroup
			select new Grouping<string, ReactiveCustomer>(sustomerGroup.Key, sustomerGroup);

            CustomersGrouped = new ObservableCollection<Grouping<string, ReactiveCustomer>>(sorted);
        }

        public CustomerListViewModel(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;

            Messenger.Default.Register<MessageData>(this, MessageData.CustomerListPageName, false, async data =>
            {
                if (data.UpdateRequired)
                    await GetCustomers();
                else
                {
                    if(data.SortRequired)
                        Sort();
                }
            });
            Messenger.Default.Send(new MessageData {UpdateRequired = true}, MessageData.CustomerListPageName);
        }
    }
}