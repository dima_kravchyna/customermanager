using System;
using CustomerManager.Core.Models;
using CustomerManager.Core.Services;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;

namespace CustomerManager.Core.ViewModels
{ 
	public class CustomerViewModel : ReactiveCustomer
	{
		private readonly ICustomerRepository _customerRepository;

		private RelayCommand _updateCustomerCommand;
        private RelayCommand _editCustomerCommand;
        private RelayCommand _deleteCustomerCommand;
        private readonly Action<CustomerViewModel> _deleteAction;
	    private readonly Func<CustomerViewModel, bool> _canDelete;

        public RelayCommand UpdateCustomerCommand
        {
            get
            {
                return _updateCustomerCommand ??
                       (_updateCustomerCommand = new RelayCommand(Update));
            }
        }

	    public RelayCommand EditCustomerCommand
	    {
	        get
	        {
	            return _editCustomerCommand ??
	                   (_editCustomerCommand = new RelayCommand(Edit));
	        }
	    }

	    public RelayCommand DeleteCustomerCommand
	    {
	        get
	        {
	            return _deleteCustomerCommand ??
	                   (_deleteCustomerCommand = new RelayCommand(
	                       () =>
	                       {
	                           if (_deleteAction != null)
	                               _deleteAction(this);
	                       },
	                       () => _canDelete == null || _canDelete(this)));
	        }
	    }


	    private async void Update()
        {
            ViewModelLocator.Locator.Customer.Model = Model;
            await _customerRepository.UpdateAsync(Model);
            Messenger.Default.Send(new MessageData { SortRequired = true }, MessageData.CustomerListPageName);
            NavigatioService.GoBack();
        }
	    private void Edit()
	    {
            ViewModelLocator.Locator.CustomerEdit = new CustomerViewModel(_customerRepository, _deleteAction, _canDelete)
                                                    { Model = Model.Clone() };
            NavigatioService.NavigateTo(MessageData.CustomerEditionPageName);
        }

        public CustomerViewModel(ICustomerRepository customerRepository, Action<CustomerViewModel> deleteAction = null, Func<CustomerViewModel, bool> canDelete = null)
        {
            _customerRepository = customerRepository;

            _deleteAction = deleteAction;
            _canDelete = canDelete;
        }
    }
}