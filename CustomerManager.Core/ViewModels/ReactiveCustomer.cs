﻿using System;
using CustomerManager.Core.Models;

namespace CustomerManager.Core.ViewModels
{
    public abstract class ReactiveCustomer : BaseViewModel
    {
        private const string DefaultPhotoUri = "http://www.iconpng.com/png/desktop-icons/company.png";

        private Customer _customer = new Customer();

        private string _name = string.Empty;
        private string _lastName = string.Empty;
        private string _position = string.Empty;
        private string _company = string.Empty;
        private string _photo = DefaultPhotoUri;
        private string _phoneNumber = string.Empty;
        private string _eMail = string.Empty;
        private string _skype = string.Empty;
        private string _streetAddress = string.Empty;
        private string _city = string.Empty;
        private string _state = string.Empty;
        private string _country = string.Empty;
        private string _zipCode = string.Empty;
        private double _latitude;
        private double _longitude;
        private string _aboutUs = string.Empty;

        ///Same lazy synchronization with _customer
        public Customer Model
        {
            get { return InitCustomer(_customer); }
            set
            {
                _customer = value;
                InitViewModel(_customer);
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (Set(() => Name, ref _name, value))
                    RaisePropertyChanged(() => FullName);
            }
        }
        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (Set(() => LastName, ref _lastName, value))
                    RaisePropertyChanged(() => FullName);
            }
        }
        public string FullName
        {
            get { return string.Format("{0} {1}", LastName, Name); }
        }
        public string Position
        {
            get { return _position; }
            set { Set(() => Position, ref _position, value); }
        }
        public string Company
        {
            get { return _company; }
            set { Set(() => Company, ref _company, value); }
        }
        public string Photo
        {
            get { return _photo; }
            set
            {
                if (Set(() => Photo, ref _photo, value))
                    RaisePropertyChanged(() => PhotoUri);
            }
        }
        public Uri PhotoUri
        {
            get { return new Uri(Photo); }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { Set(() => PhoneNumber, ref _phoneNumber, value); }
        }
        public string EMail
        {
            get { return _eMail; }
            set { Set(() => EMail, ref _eMail, value); }
        }
        public string Skype
        {
            get { return _skype; }
            set { Set(() => Skype, ref _skype, value); }
        }
        public string StreetAddress
        {
            get { return _streetAddress; }
            set { Set(() => StreetAddress, ref _streetAddress, value); }
        }
        public string City
        {
            get { return _city; }
            set { Set(() => City, ref _city, value); }
        }
        public string State
        {
            get { return _state; }
            set { Set(() => State, ref _state, value); }
        }
        public string Country
        {
            get { return _country; }
            set { Set(() => Country, ref _country, value); }
        }
        public string ZipCode
        {
            get { return _zipCode; }
            set { Set(() => ZipCode, ref _zipCode, value); }
        }
        public double Latitude
        {
            get { return _latitude; }
            set { Set(() => Latitude, ref _latitude, value); }
        }
        public double Longitude
        {
            get { return _longitude; }
            set { Set(() => Longitude, ref _longitude, value); }
        }
        public string AboutUs
        {
            get { return _aboutUs; }
            set { Set(() => AboutUs, ref _aboutUs, value); }
        }

        private void InitViewModel(Customer customer)
        {
            Name = customer.Name;
            LastName = customer.LastName;
            Position = customer.Position;
            Company = customer.Company;
            Photo = customer.Photo;
            PhoneNumber = customer.PhoneNumber;
            EMail = customer.EMail;
            Skype = customer.Skype;
            StreetAddress = customer.StreetAddress;
            City = customer.City;
            State = customer.State;
            Country = customer.Country;
            ZipCode = customer.ZipCode;
            Latitude = customer.Latitude;
            Longitude = customer.Longitude;
            AboutUs = customer.AboutUs;
        }
        private Customer InitCustomer(Customer customer)
        {
            customer.Name = Name;
            customer.LastName = LastName;
            customer.Position = Position;
            customer.Company = Company;
            customer.Photo = Photo;
            customer.PhoneNumber = PhoneNumber;
            customer.EMail = EMail;
            customer.Skype = Skype;
            customer.StreetAddress = StreetAddress;
            customer.City = City;
            customer.State = State;
            customer.Country = Country;
            customer.ZipCode = ZipCode;
            customer.Latitude = Latitude;
            customer.Longitude = Longitude;
            customer.AboutUs = AboutUs;
            return customer;
        }
    }
}
