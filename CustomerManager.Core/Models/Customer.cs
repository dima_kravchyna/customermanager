﻿namespace CustomerManager.Core.Models
{
	public class Customer
	{
	    public string Id { get;  set; }
		public string Name { get; set; }
		public string LastName { get; set; }
		public string Position { get; set; }
		public string Company { get; set; }
		public string Photo { get; set; }
        public string PhoneNumber { get; set; }
		public string EMail { get; set; }
		public string Skype { get; set; }
		public string StreetAddress { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Country { get; set; }
		public string ZipCode { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public string AboutUs { get; set; }

        public Customer()
        {
            Name = string.Empty;
            LastName = string.Empty;
            Position = string.Empty;
            Company = string.Empty;
            Photo = string.Empty;
            PhoneNumber = string.Empty;
            EMail = string.Empty;
            Skype = string.Empty;
            StreetAddress = string.Empty;
            City = string.Empty;
            State = string.Empty;
            Country = string.Empty;
            ZipCode = string.Empty;
            AboutUs = string.Empty;
        }

        public Customer Clone()
	    {
	        return new Customer()
	        {
	            Id = Id,
                Name = Name,
                LastName = LastName,
                Position = Position,
                Company = Company,
                Photo = Photo,
                PhoneNumber = PhoneNumber,
                EMail = EMail,
                Skype = Skype,
                StreetAddress = StreetAddress,
                City = City,
                State = State,
                Country = Country,
                ZipCode = ZipCode,
                Latitude = Latitude,
                Longitude = Longitude,
                AboutUs = AboutUs
	        };
	    }
	}
}