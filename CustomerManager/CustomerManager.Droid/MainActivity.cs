﻿using Android.App;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace CustomerManager.Droid
{
    [Activity(Label = "CustomerManager", Icon = "@drawable/icon", MainLauncher = true,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            Xamarin.Forms.Forms.Init(this, bundle);
            ImageCircle.Forms.Plugin.Droid.ImageCircleRenderer.Init();
            //global::Xamarin.FormsMaps.Init(this, bundle);
            //Microsoft.WindowsAzure.MobileServices.CurrentPlatform.Init();
            //Xamarin.Insights.Initialize("c3d88c6f124fdabdf8880b65845094bb7bad90ac", this);
            //Xamarin.Insights.ForceDataTransmission = true;

            LoadApplication(new UI.Forms.App());
            //ImageCircleRenderer.Init();
            //ActionBar.SetIcon(new ColorDrawable(Resources.GetColor(Android.Resource.Color.Transparent)));

        }
    }
}

