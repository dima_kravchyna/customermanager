﻿using ImageCircle.Forms.Plugin.WindowsPhone;
using Microsoft.Phone.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;

namespace CustomerManager.WinPhone
{
	public partial class MainPage : FormsApplicationPage
	{
		public MainPage ()
		{
			InitializeComponent ();
			SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;

            Forms.Init();
            ImageCircleRenderer.Init();

            LoadApplication(new UI.Forms.App());
		}
	}
}
