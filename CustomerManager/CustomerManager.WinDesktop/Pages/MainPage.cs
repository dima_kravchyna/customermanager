﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CustomerManager.Core.Services;
using CustomerManager.Core.ViewModels;
using CustomerManager.WinDesktop.Services;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;

namespace CustomerManager.WinDesktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainPage : Window
    {
        public MainPage()
        {
            var nav = new WpfNavigationService();
            nav.Configure(MessageData.MainPageName, typeof(MainPage));
            nav.Configure(MessageData.CustomerListPageName, typeof(CustomerListPage));
            nav.Configure(MessageData.CustomerInfoPageName, typeof(CustomerPage));
            nav.Configure(MessageData.CustomerEditionPageName, typeof(CustomerEditPage));
            SimpleIoc.Default.Register<INavigationService>(() => nav);

            InitializeComponent();

            nav.Initialize(navigationFrame.NavigationService);
        }
    }
}
