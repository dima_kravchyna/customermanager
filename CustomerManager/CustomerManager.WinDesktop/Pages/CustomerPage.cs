﻿using System.Windows;
using System.Windows.Controls;
using CustomerManager.Core.ViewModels;

namespace CustomerManager.WinDesktop
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class CustomerPage : Page
    {
        public CustomerPage()
        {
            InitializeComponent();
        }
    }
}
